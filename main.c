#include <stdio.h>
#include <windows.h>
#include <stdbool.h>

#define pion "X"

#define arrowDown 80
#define arrowUp 72
#define arrowRight 77
#define arrowLeft 75

#define W 119
#define S 115
#define A 97
#define D 100

typedef struct
{
    int baris, kolom;
} table;

typedef struct
{
    COORD koordinat;
    table Resolution;
} area;

table ReadResolution();
void gotoxy(short int x, short int y);
void tampil(area Data);
void clear(COORD koordinat);

int main()
{

    area Data;
    COORD tempCord;
    Data.Resolution = ReadResolution();
    Data.koordinat.X = ReadResolution().kolom / 2;
    Data.koordinat.Y = ReadResolution().baris / 2;

start:
    tampil(Data);
    tempCord = Data.koordinat;
    input(&Data);
    clear(tempCord);
    goto start;
    return 0;
}

void tampil(area Data)
{
    gotoxy(Data.koordinat.X, Data.koordinat.Y);
    printf(pion);
}
void clear(COORD koordinat)
{
    gotoxy(koordinat.X, koordinat.Y);
    printf(" ");
}
void input(area *Data)
{
    bool exit = false;
    int temp;
    COORD TempCoord = Data->koordinat;
    while (exit == false)
    {
        temp = getch();
        if (temp == arrowDown || temp == S)
        {
            Data->koordinat.Y++;
            exit = true;
        }
        else if (temp == arrowUp || temp == W)
        {
            Data->koordinat.Y--;
            exit = true;
        }
        else if (temp == arrowLeft || temp == A)
        {
            Data->koordinat.X--;
            exit = true;
        }
        else if (temp == arrowRight || temp == D)
        {
            Data->koordinat.X++;
            exit = true;
        }

        if (Data->koordinat.X >= Data->Resolution.kolom || Data->koordinat.Y >= Data->Resolution.baris || Data->koordinat.X < 0 || Data->koordinat.Y < 0)
        {
            exit = false;
            Data->koordinat = TempCoord;
        }
    }
}

table ReadResolution()
{
    table Result;

    CONSOLE_SCREEN_BUFFER_INFO csbi;
    GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi);
    Result.kolom = csbi.srWindow.Right - csbi.srWindow.Left + 1;
    Result.baris = csbi.srWindow.Bottom - csbi.srWindow.Top + 1;

    return Result;
}

void gotoxy(short int x, short int y)
{
    HANDLE hConsoleOutput;
    COORD dwCursorPosition;
    dwCursorPosition.X = x;
    dwCursorPosition.Y = y;
    hConsoleOutput = GetStdHandle(STD_OUTPUT_HANDLE);
    //? Initialisasi

    SetConsoleCursorPosition(hConsoleOutput, dwCursorPosition);
    // Set kursor ke lokasi
}